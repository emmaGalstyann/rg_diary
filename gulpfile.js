var gulp = require('gulp'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    rigger = require('gulp-rigger'),
    browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

var cssFiles = [
    'node_modules/normalize.css/normalize.css',
    'app/css/style.css'
];
var jsFiles = [
    'app/js/style.js'
    
];
var fileFont = [
    'app/fonts/**/*.*'
];


function styles(){ 
     return gulp.src(cssFiles)
    .pipe(concat('all.css'))
    .pipe(autoprefixer({
         browsers: ['> 0.1%'],
         cascade: false
     }))
    .pipe(cleanCSS({
         level: 2
     }))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream());
}
function scripts(){ 
     return gulp.src(jsFiles)
    .pipe(concat('all.js'))
    .pipe(uglify({
         toplevel: 2
     }))
    .pipe(gulp.dest('dist/js'))
    .pipe(browserSync.stream());
}
function clean(){
    return del(['dist/*']);
}

gulp.task('sass', function(){
    return gulp.src('app/sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.stream());
});
/*
gulp.task('html:build', function () {
    gulp.src('*.html') //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest('*.html')) //Выплюнем их в папку build
        .pipe(browserSync.stream()); //И перезагрузим наш сервер для обновлений
});*/

gulp.task('styles', styles);
gulp.task('scripts', scripts);

gulp.task('font', function() {
    return gulp.src(fileFont)
    .pipe(gulp.dest('dist/fonts'))
    .pipe(browserSync.stream());
}); 
gulp.task('libs', function() {
    return gulp.src('app/libs/**/*.*')
    .pipe(gulp.dest('dist/libs'))
    .pipe(browserSync.stream());
}); 

gulp.task('images', function(){
   return gulp.src('app/img/*.*')
        .pipe(imagemin([
       imagemin.gifsicle({interlaced: true}),
       imagemin.jpegtran({progressive: true}),
       imagemin.optipng({optimizationLevel: 5}),
       imagemin.svgo({plugins: [{removeViewBox: true}]})
   ]))
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function(){
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('app/fonts/**/*.*', gulp.parallel('font'));
    gulp.watch('app/libs/**/*.*', gulp.parallel('libs'));
    gulp.watch('app/sass/**/*.scss', gulp.parallel('sass'));
    gulp.watch('app/css/**/*.css', gulp.parallel('styles'));
    gulp.watch('app/js/**/*.js', gulp.parallel('scripts'));
    gulp.watch('app/img/*', gulp.parallel('images'));
    gulp.watch("*.html").on("change", reload);
});

//gulp.task('watch', function(){
//    gulp.watch('app/sass/**/*.scss', gulp.parallel('sass'));
//    
//});

gulp.task('dist', gulp.series(clean,
                  gulp.parallel('watch', 'styles', 'scripts', 'images')
          ));