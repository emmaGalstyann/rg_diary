$(document).ready(function() {
  /* menu */
  /*$('nav li a').on('click', function(event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id = $(this).attr('href'),
      //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    //анимируем переход на расстояние - top за 1000 мс
    $('html, body')
      .stop()
      .animate(
        {
          scrollTop: top,
          easing: 'easein',
        },
        1000
      );
  });*/
  $('.mobile-icon').on('click', function() {
    $(this)
      .toggleClass('active')
      .parents('header')
      .toggleClass('menu-open')
      .parents('body')
      .toggleClass('overflow');
  });

  var fixedElem1 = new TimelineMax({ repeat: -1 });
  fixedElem1.fromTo(
    $('.fixed-block .elem5'),
    8,
    { opacity: 1, y: 200 },
    { opacity: 0, y: -1000, ease: Linear.easeOut }
  );

  var fixedElem2 = new TimelineMax({ repeat: -1 });
  fixedElem2.fromTo(
    $('.fixed-block .elem6'),
    10,
    { opacity: 1, y: 200 },
    { opacity: 0, y: -1000, ease: Linear.easeOut }
  );

  var fixedElem3 = new TimelineMax({ repeat: -1 });
  fixedElem3.fromTo(
    $('.fixed-block .elem7'),
    15,
    { opacity: 1, y: 200 },
    { opacity: 0, y: -1000, ease: Linear.easeOut }
  );

  var fixedElem4 = new TimelineMax({ repeat: -1 });
  fixedElem4.fromTo(
    $('.fixed-block .elem8'),
    6,
    { opacity: 1, y: 200 },
    { opacity: 0, y: -1000, ease: Linear.easeOut }
  );

  var fixedElem5 = new TimelineMax({ repeat: -1 });
  fixedElem5.fromTo(
    $('.fixed-block .elem9'),
    9,
    { opacity: 1, y: 200 },
    { opacity: 0, y: -1000, ease: Linear.easeOut }
  );
});

if ($(window).width() > 768) {
  var ctrl = new ScrollMagic.Controller({});

  var tl1 = new TimelineMax();
  tl1.fromTo(
    $('.first-screen .container'),
    0.2,
    { opacity: 1 },
    { opacity: 0, ease: Linear.easeOut }
  );

  var scene1 = new ScrollMagic.Scene({
    triggerElement: '.trigger1',
    triggerHook: 0.3,
    duration: '40%',
  })
    .setTween(tl1)
    .addTo(ctrl);

  var tl2 = new TimelineMax();
  tl2
    .fromTo(
      $('.section2 .img'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut }
    )
    .fromTo(
      $('.section2 .text-block'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut },
      '-=0.2'
    );

  var scene2 = new ScrollMagic.Scene({
    triggerElement: '.section2',
    triggerHook: 0.3,
    duration: '50%',
  })
    .setPin('.section2')
    .setTween(tl2)
    .addTo(ctrl);

  var tl3 = new TimelineMax();
  tl3.fromTo(
    $('.section2 .container'),
    0.2,
    { opacity: 1, y: 0 },
    { opacity: 0, y: 200, ease: Linear.easeOut }
  );

  var scene3 = new ScrollMagic.Scene({
    triggerElement: '.section2 .trigger1',
    triggerHook: 0.7,
    duration: '50%',
  })
    .setTween(tl3)
    .addTo(ctrl);

  var tl4 = new TimelineMax();
  tl4
    .fromTo(
      $('.section3 .text-block'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut }
    )
    .fromTo(
      $('.section3 .img'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut },
      '-=0.2'
    );

  var scene4 = new ScrollMagic.Scene({
    triggerElement: '.section3',
    triggerHook: 0.2,
    duration: '50%',
  })
    .setPin('.section3')
    .setTween(tl4)
    .addTo(ctrl);

  var tl5 = new TimelineMax();
  tl5.fromTo(
    $('.section3 .container'),
    0.2,
    { opacity: 1, y: 0 },
    { opacity: 0, y: 100, ease: Linear.easeOut }
  );

  var scene5 = new ScrollMagic.Scene({
    triggerElement: '.section3 .trigger1',
    triggerHook: 0.7,
    duration: '50%',
  })
    .setTween(tl5)
    .addTo(ctrl);

  var tl6 = new TimelineMax();
  tl6
    .fromTo(
      $('.section4 .book-anima-first'),
      0.3,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut }
    )
    .fromTo(
      $('.section4 .book-open'),
      0.3,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut },
      '-=0.3'
    );
  /*
  .fromTo(
    $('.section4 .section4-block1'),
    0.1,
    { opacity: 1, y: 0 },
    { opacity: 0, y: -100, display: 'none', ease: Linear.easeOut }
  )
  .fromTo(
    $('.section4 .section4-block2'),
    0.4,
    { y: 500 },
    { y: 0, ease: Linear.easeOut },
    '-=0.1'
  )
  .staggerFromTo(
    $('.section4 .section4-block2 .info'),
    0.7,
    { opacity: 0.5, y: 150 },
    {
      opacity: 1,
      y: 0,
      ease: Linear.easeOut,
      onComplete: tweenComplete,
      onCompleteParams: ['{self}'],
    },
    0.1,
    '-=0.1'
  );

function tweenComplete(tween) {
  $('.section4 .section4-block2 .info').removeClass('active');
  tween.target.className = 'info active';
}
*/

  var scene6 = new ScrollMagic.Scene({
    triggerElement: '.section4',
    triggerHook: 0.4,
    duration: '50%',
  })
    .setPin('.section4')
    .setTween(tl6)
    .addTo(ctrl);

  var tl7 = new TimelineMax();
  tl7.fromTo(
    $('.section4 .container'),
    0.4,
    { opacity: 1, y: 0 },
    { opacity: 0, y: 100, ease: Linear.easeOut }
  );

  var scene7 = new ScrollMagic.Scene({
    triggerElement: '.section4 .trigger1',
    triggerHook: 0.5,
    duration: '70%',
  })
    .setTween(tl7)
    //.addIndicators()
    .addTo(ctrl);

  var tl8 = new TimelineMax();
  tl8
    .staggerFromTo(
      $('.section5 .text-right'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut }
    )
    .fromTo(
      $('.section5 .text-left'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut },
      '-=0.2'
    );

  var scene8 = new ScrollMagic.Scene({
    triggerElement: '.section5',
    triggerHook: 0.4,
    duration: '50%',
  })
    .setPin('.section5')
    .setTween(tl8)
    //.addIndicators()
    .addTo(ctrl);

  var tl9 = new TimelineMax();
  tl9.fromTo(
    $('.section5 .container'),
    0.4,
    { opacity: 1, y: 0 },
    { opacity: 0, y: 100, ease: Linear.easeOut }
  );

  var scene9 = new ScrollMagic.Scene({
    triggerElement: '.section5 .trigger1',
    triggerHook: 0.7,
    duration: '50%',
  })
    .setTween(tl9)
    //.addIndicators()
    .addTo(ctrl);

  var tl10 = new TimelineMax();
  tl10
    .fromTo(
      $('.articles .article'),
      0.6,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut }
    )
    .fromTo(
      $('.articles .btn'),
      0.1,
      { y: 200 },
      { y: 0, ease: Linear.easeOut },
      '-=0.2'
    );

  var scene10 = new ScrollMagic.Scene({
    triggerElement: '.articles',
    triggerHook: 0.5,
    duration: '50%',
  })
    .setPin('.articles')
    .setTween(tl10)
    //.addIndicators()
    .addTo(ctrl);

  var tl11 = new TimelineMax();
  tl11.fromTo(
    $('.articles'),
    0.3,
    { opacity: 1, y: 0 },
    { opacity: 0, y: 100, ease: Linear.easeOut }
  );

  var scene11 = new ScrollMagic.Scene({
    triggerElement: '.articles .trigger1',
    triggerHook: 0.9,
    duration: '40%',
  })
    .setTween(tl11)
    //.addIndicators()
    .addTo(ctrl);

  var tl12 = new TimelineMax();
  tl12.fromTo(
    $('.instagram'),
    0.2,
    { opacity: 0, y: 300 },
    { opacity: 1, y: 0, ease: Linear.easeOut }
  );

  var scene12 = new ScrollMagic.Scene({
    triggerElement: '.instagram-section',
    triggerHook: 0.3,
    duration: '50%',
  })
    .setPin('.instagram-section')
    .setTween(tl12)
    //.addIndicators()
    .addTo(ctrl);

  var tl13 = new TimelineMax();
  tl13.fromTo(
    $('.instagram-section'),
    0.3,
    { opacity: 1, y: 0 },
    { opacity: 0, y: 100, ease: Linear.easeOut }
  );

  var scene13 = new ScrollMagic.Scene({
    triggerElement: '.instagram-section .trigger1',
    triggerHook: 0.4,
    duration: '40%',
  })
    .setTween(tl13)
    //.addIndicators()
    .addTo(ctrl);

  var tl14 = new TimelineMax();
  tl14
    .fromTo(
      $('.contact .text'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut }
    )
    .fromTo(
      $('.contact .info-block'),
      0.2,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut },
      '-=0.2'
    )
    .fromTo(
      $('.contact form'),
      0.3,
      { opacity: 0, y: 200 },
      { opacity: 1, y: 0, ease: Linear.easeOut },
      '-=0.2'
    );

  var scene14 = new ScrollMagic.Scene({
    triggerElement: '.contact',
    triggerHook: 0.4,
    duration: '50%',
  })
    .setPin('.contact')
    .setTween(tl14)
    //.addIndicators()
    .addTo(ctrl);

  var tl15 = new TimelineMax();
  tl15.fromTo(
    $('.contact-section .container'),
    0.3,
    { opacity: 1, y: 0 },
    { opacity: 0, y: -200, ease: Linear.easeOut }
  );

  var scene15 = new ScrollMagic.Scene({
    triggerElement: '.contact-section .trigger1',
    triggerHook: 0.6,
    duration: '40%',
  })
    .setTween(tl15)
    //.addIndicators()
    .addTo(ctrl);

  var tl16 = new TimelineMax();
  tl16.staggerFromTo(
    $('footer > div'),
    0.2,
    { opacity: 0, y: 100 },
    { opacity: 1, y: 0, ease: Linear.easeOut }
  );

  var scene16 = new ScrollMagic.Scene({
    triggerElement: 'footer',
    triggerHook: 0.5,
    duration: '30%',
  })
    .setTween(tl16)
    //.addIndicators()
    .addTo(ctrl);
}
